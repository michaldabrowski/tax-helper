#!/bin/bash

if [ $DEBUG_ENABLED -eq 1 ]
then
  exec java \
	-Xmx${JVM_XMX} \
	-Xms${JVM_XMS} \
	-agentlib:jdwp=transport=dt_socket,server=y,suspend=n,address=*:9009 \
	-jar /opt/apps/tax-helper.jar
else
  exec java \
	-Xmx${JVM_XMX} \
	-Xms${JVM_XMS} \
	-jar /opt/apps/tax-helper.jar
fi
