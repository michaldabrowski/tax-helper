#!/bin/bash

function die {
	echo '-r - recreate database'
	exit 0
}

APP_CONTAINER_NAME="tax-helper-app"
DB_CONTAINER_NAME="tax-helper-db"
RECREATE_DB="false"

while getopts "r" OPTION
do
     case $OPTION in
         r)
            RECREATE_DB="true"
            ;;
         ?)
            die
            ;;
     esac
done

function removeContainer(){
    CONTAINER=$1
    RUNNING=`docker inspect -f {{.State.Running}} $CONTAINER 2> /dev/null`
        if [ "$RUNNING" == "true" ]; then
            echo "Stopping $CONTAINER" && \
            docker stop "$CONTAINER"
        fi
    echo "Removing $CONTAINER if exists" && \
    docker rm "$CONTAINER" 2> /dev/null
}

removeContainer APP_CONTAINER_NAME

if [ "$RECREATE_DB" == "true" ]; then
    removeContainer $DB_CONTAINER_NAME
fi

echo "Rebuilding application" && \
mvn clean package && \

echo "Copying archive to docker directory" && \
mkdir -p docker/temp && \
cp target/tax-helper.jar docker/temp/ && \

echo  "Rebuilding docker image" && \
cd docker && \
docker build --rm -t app/tax-helper:1.0 . && \

rm -fr temp && \

echo "Running docker-compose" && \
docker-compose up
docker-compose logs -f
