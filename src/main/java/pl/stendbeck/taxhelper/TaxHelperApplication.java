package pl.stendbeck.taxhelper;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TaxHelperApplication {

    public static void main(String[] args) {
        SpringApplication.run(TaxHelperApplication.class, args);
    }
}
