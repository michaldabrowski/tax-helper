package pl.stendbeck.taxhelper.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import pl.stendbeck.taxhelper.service.ZusContributionService;

import java.time.LocalDate;

@RestController
public class ChargeController {

    @Autowired
    private ZusContributionService zusService;

    @PostMapping("/v1/zus/{zusId}/pay")
    public ResponseEntity<Void> payZus(@PathVariable long zusId,
                                       @RequestParam @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate date) {
        zusService.payZus(zusId, date);
        return ResponseEntity.ok().build();
    }
}
