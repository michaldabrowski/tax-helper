package pl.stendbeck.taxhelper.model;

public enum InvoiceType {
    INCOME, EXPENSE
}
