package pl.stendbeck.taxhelper.model.vo;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import pl.stendbeck.taxhelper.model.InvoiceType;
import pl.stendbeck.taxhelper.model.TaxValue;
import pl.stendbeck.taxhelper.model.VatValue;

import java.math.BigDecimal;
import java.time.LocalDate;

@Getter
@Setter
@NoArgsConstructor
public class InvoiceVO {

    private String customer;
    private InvoiceType type;
    private LocalDate date;
    private BigDecimal amount;
    private VatValue vatValue;
    private TaxValue taxValue;
    private String description;
}
