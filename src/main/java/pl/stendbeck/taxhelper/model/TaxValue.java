package pl.stendbeck.taxhelper.model;

import java.math.BigDecimal;

public enum TaxValue {
    _100_PERCENT(1), _75_PERCENT(0.75);

    private double percent;

    TaxValue(double percent){
        this.percent = percent;
    }

    public BigDecimal calculateTaxBase(BigDecimal amount){
        return amount.multiply(BigDecimal.valueOf(percent));
    }
}
