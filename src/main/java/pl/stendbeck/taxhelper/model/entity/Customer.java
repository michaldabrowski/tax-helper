package pl.stendbeck.taxhelper.model.entity;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;

@Data
@Entity
@SequenceGenerator(name = "CustomerSequence", sequenceName = "seq_customer", allocationSize = 1)
public class Customer implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "CustomerSequence")
    private Long id;

    private String login;

    private String name;

    public Customer() {
    }


}
