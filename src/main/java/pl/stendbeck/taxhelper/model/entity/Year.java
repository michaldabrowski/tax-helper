package pl.stendbeck.taxhelper.model.entity;

import lombok.Data;
import pl.stendbeck.taxhelper.model.IncomeTaxType;

import javax.persistence.*;
import java.io.Serializable;

@Data
@Entity
public class Year implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "customer_id")
    private Customer customer;

    private int value;

    @Enumerated(EnumType.STRING)
    private IncomeTaxType taxType;
}
