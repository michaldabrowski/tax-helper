package pl.stendbeck.taxhelper.model.entity;

import lombok.Data;
import pl.stendbeck.taxhelper.model.InvoiceType;
import pl.stendbeck.taxhelper.model.TaxValue;
import pl.stendbeck.taxhelper.model.VatValue;
import pl.stendbeck.taxhelper.model.vo.InvoiceVO;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;

@Data
@Entity
@SequenceGenerator(name = "InvoiceSequence", sequenceName = "seq_invoice", allocationSize = 1)
public class Invoice implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "InvoiceSequence")
    private Long id;

    @ManyToOne
    @JoinColumn(name = "customer_id")
    private Customer customer;

    @ManyToOne
    @JoinColumn(name = "month_id")
    private Month month;

    @Enumerated(EnumType.STRING)
    private InvoiceType type;

    private LocalDate date;

    private BigDecimal amount;

    @Enumerated(EnumType.STRING)
    private VatValue vatValue;

    @Enumerated(EnumType.STRING)
    private TaxValue taxValue;

    private String description;

    public static Invoice assembly(InvoiceVO vo){
        Invoice invoice = new Invoice();
        invoice.setDescription(vo.getDescription());
        invoice.setTaxValue(vo.getTaxValue());
        invoice.setVatValue(vo.getVatValue());
        invoice.setAmount(vo.getAmount());
        invoice.setType(vo.getType());
        invoice.setDate(vo.getDate());
        return invoice;
    }


    public BigDecimal getTaxBase(){
        BigDecimal base = amount.subtract(getVat());
        return taxValue.calculateTaxBase(base);
    }

    public BigDecimal getVat(){
        return vatValue.getVatAmount(amount);
    }
}
