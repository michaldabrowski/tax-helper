package pl.stendbeck.taxhelper.model.entity;

import lombok.Data;
import pl.stendbeck.taxhelper.model.ChargeStatus;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;

@Data
@Entity
@SequenceGenerator(name = "ZusContributionSequence", sequenceName = "seq_zus_contribution", allocationSize = 1)
public class ZusContribution implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "ZusContributionSequence")
    private Long id;

    @ManyToOne
    @JoinColumn(name = "customer_id")
    private Customer customer;

    @ManyToOne
    @JoinColumn(name = "month_id")
    private Month month;

    private LocalDate paymentDate;

    private LocalDate termDate;

    @Enumerated(EnumType.STRING)
    private ChargeStatus status;

    private BigDecimal socialAmount = BigDecimal.ZERO;

    private BigDecimal healthCareAmount = BigDecimal.ZERO;

    private BigDecimal LaborFundAmount = BigDecimal.ZERO;


}
