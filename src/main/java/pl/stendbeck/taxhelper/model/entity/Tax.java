package pl.stendbeck.taxhelper.model.entity;

import lombok.Data;
import pl.stendbeck.taxhelper.model.ChargeStatus;
import pl.stendbeck.taxhelper.model.TaxType;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;

@Data
@Entity
@SequenceGenerator(name = "TaxSequence", sequenceName = "seq_tax", allocationSize = 1)
public class Tax implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "TaxSequence")
    private Long id;

    @ManyToOne
    @JoinColumn(name = "customer_id")
    private Customer customer;

    @ManyToOne
    @JoinColumn(name = "month_id")
    private Month month;

    private LocalDate paymentDate;

    private LocalDate termDate;

    @Enumerated(EnumType.STRING)
    private TaxType type;

    @Enumerated(EnumType.STRING)
    private ChargeStatus status;

    private BigDecimal amount = BigDecimal.ZERO;
}
