package pl.stendbeck.taxhelper.model;

import java.math.BigDecimal;


public enum IncomeTaxType {
    PROGRESSIVE {
        @Override
        public BigDecimal calculateTax(BigDecimal baseAmount, BigDecimal firstTaxThreshold) {
            if(baseAmount.compareTo(firstTaxThreshold) <= 0){
                return baseAmount.multiply(MULTIPLICAND_18);
            }

            BigDecimal firstThresholdTaxResult = firstTaxThreshold.multiply(MULTIPLICAND_18);
            BigDecimal secondThresholdTaxResult = baseAmount.subtract(firstTaxThreshold).multiply(MULTIPLICAND_32);
            return firstThresholdTaxResult.add(secondThresholdTaxResult);
        }
    },

    FLAT {
        @Override
        public BigDecimal calculateTax(BigDecimal baseAmount, BigDecimal firstTaxThreshold) {
            return baseAmount.multiply(MULTIPLICAND_19);
        }
    };

    private static BigDecimal MULTIPLICAND_18 = new BigDecimal("0.18");
    private static BigDecimal MULTIPLICAND_19 = new BigDecimal("0.19");
    private static BigDecimal MULTIPLICAND_32 = new BigDecimal("0.32");

    public abstract BigDecimal calculateTax(BigDecimal baseAmount, BigDecimal firstTaxThreshold);
}
