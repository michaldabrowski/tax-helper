package pl.stendbeck.taxhelper.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.stendbeck.taxhelper.model.entity.Customer;
import pl.stendbeck.taxhelper.model.entity.Invoice;
import pl.stendbeck.taxhelper.model.entity.Month;
import pl.stendbeck.taxhelper.model.vo.InvoiceVO;
import pl.stendbeck.taxhelper.repository.InvoiceRepository;

import javax.transaction.Transactional;

@Service
@Transactional
public class InvoiceServiceImpl implements InvoiceService {


    @Autowired
    private InvoiceRepository invoiceRepository;

    @Autowired
    private CustomerService customerService;

    @Autowired
    private PeriodService periodService;

    @Autowired
    private TaxService taxService;

    @Override
    public void addInvoice(InvoiceVO invoiceVO) {

        Customer customer = customerService.findByLogin(invoiceVO.getCustomer());
        Month month = periodService.findMonthByDate(invoiceVO.getDate());

        Invoice invoice = new Invoice();
        invoice.setCustomer(customer);
        invoice.setMonth(month);
        invoice.setDate(invoiceVO.getDate());
        invoice.setType(invoiceVO.getType());
        invoice.setAmount(invoiceVO.getAmount());
        invoice.setDescription(invoiceVO.getDescription());
        invoice.setTaxValue(invoiceVO.getTaxValue());
        invoice.setVatValue(invoiceVO.getVatValue());

        invoiceRepository.save(invoice);

        taxService.calculateMonth(month, false);

    }


//    public void calculate(long monthId) {
//        System.out.println("OBLICZAM DLA " + monthId);
//
//
//        Optional<Month> month = monthRepository.findById(monthId);
//        if (!month.isPresent()) {
//            System.out.println("WYBRANY MIESIĄC NIE ISTNIEJE");
//        }
//
//        IncomeTaxType taxType = month.get().getYear().getTaxType();
//
//        List<Invoice> invoices = invoiceRepository.findByMonthId(month.get().getId());
//
//        System.out.println("SIZE:" + invoices.size());
//
//        BigDecimal base = BigDecimal.ZERO;
//        BigDecimal vat = BigDecimal.ZERO;
//
//        for (Invoice invoice : invoices) {
//            if (invoice.getType() == INCOME) {
//                base = base.add(invoice.getTaxBase());
//                vat = vat.add(invoice.getVat());
//            } else {
//                base = base.subtract(invoice.getTaxBase());
//                vat = vat.subtract(invoice.getVat());
//            }
//        }
//
//
//        base = base.subtract(new BigDecimal("175.92")).setScale(0, RoundingMode.HALF_UP);
//
//
//        if (taxType == PROGRESSIVE) {
//            base = base.multiply(new BigDecimal("0.18"));
//        } else {
//            base = base.multiply(new BigDecimal("0.19"));
//        }
//
//        BigDecimal zdrowotna = new BigDecimal("297.28").divide(new BigDecimal("9"), 2, RoundingMode.HALF_UP);
//        zdrowotna = zdrowotna.multiply(new BigDecimal("7.75").setScale(2, RoundingMode.HALF_UP));
//
//        base = base.subtract(zdrowotna);
//
//        base = base.subtract(new BigDecimal("556.02").setScale(0, RoundingMode.HALF_UP));
//
//
//        base = base.setScale(0, RoundingMode.HALF_UP);
//
//        System.out.println("****************************************");
//        System.out.println(base.toString());
//        System.out.println("****************************************");
//
//
////        przychód - koszty - społeczne = DOCHOD - ZAOKRAGLIĆ DO 0
////        DOCHÓD * 19% = PODATEK_1
////        ZDROWOTNA = składki zdrowotne / 9 * 7,75  - ZAOKRAGLIĆ do 2 MIEJSC
////        PODATEK_1 - ZDROWOTNA
////        WSZYSTKO ZAOKRĄGLIĆ do 0
//
//
//    }


}
