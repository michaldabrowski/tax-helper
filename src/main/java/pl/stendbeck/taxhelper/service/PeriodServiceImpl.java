package pl.stendbeck.taxhelper.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.stendbeck.taxhelper.model.entity.Customer;
import pl.stendbeck.taxhelper.model.entity.Month;
import pl.stendbeck.taxhelper.model.entity.Year;
import pl.stendbeck.taxhelper.model.vo.CreateMonthVO;
import pl.stendbeck.taxhelper.repository.MonthRepository;
import pl.stendbeck.taxhelper.repository.YearRepository;

import javax.transaction.Transactional;
import java.time.LocalDate;

import static pl.stendbeck.taxhelper.model.PeriodStatus.ACTIVE;

@Service
@Transactional
public class PeriodServiceImpl implements PeriodService {

    @Autowired
    private CustomerService customerService;

    @Autowired
    private TaxService taxService;

    @Autowired
    private ZusContributionService zusContributionService;

    @Autowired
    private YearRepository yearRepository;

    @Autowired
    private MonthRepository monthRepository;

    @Override
    public void createMonth(CreateMonthVO monthVO) {
        Customer customer = customerService.findByLogin(monthVO.getCustomer());
        Year year = yearRepository.findByValue(monthVO.getYear());

        Month month = new Month();
        month.setCustomer(customer);
        month.setYear(year);
        month.setName(monthVO.getName());
        month.setDateFrom(monthVO.getDateFrom());
        month.setDateTo(monthVO.getDateTo());
        month.setStatus(ACTIVE);

        monthRepository.save(month);

        taxService.createTaxesForMonth(customer, month);
        zusContributionService.createZusContributions(customer, month);
    }

    @Override
    public void closeMonth(long monthId) {
        Month month = monthRepository.findById(monthId).orElseThrow();
        taxService.calculateMonth(month, true);
    }

    @Override
    public Month findMonthByDate(LocalDate date) {
        return monthRepository.findByDate(date).orElseThrow();
    }
}
