package pl.stendbeck.taxhelper.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import pl.stendbeck.taxhelper.model.entity.Customer;
import pl.stendbeck.taxhelper.model.entity.Month;
import pl.stendbeck.taxhelper.model.entity.ZusContribution;
import pl.stendbeck.taxhelper.repository.ZusContributionRepository;

import javax.transaction.Transactional;
import java.math.BigDecimal;
import java.time.LocalDate;

import static pl.stendbeck.taxhelper.model.ChargeStatus.CLOSED;
import static pl.stendbeck.taxhelper.model.ChargeStatus.PAID;

@Service
@Transactional
public class ZusContributionServiceImpl implements ZusContributionService {

    @Value("${zus.term-day-of-month}")
    private int zusTermDayOfMonth;

    @Value("${zus.contributions.social}")
    private BigDecimal social;

    @Value("${zus.contributions.health-care}")
    private BigDecimal healthCare;

    @Value("${zus.contributions.labor-fund}")
    private BigDecimal laborFund;

    @Autowired
    private ZusContributionRepository zusContributionRepository;


    @Override
    public void createZusContributions(Customer customer, Month month) {
        ZusContribution zus = new ZusContribution();
        zus.setCustomer(customer);
        zus.setMonth(month);
        zus.setStatus(CLOSED);
        zus.setTermDate(month.getDateFrom().plusMonths(1L).withDayOfMonth(zusTermDayOfMonth));
        zus.setSocialAmount(social);
        zus.setHealthCareAmount(healthCare);
        zus.setLaborFundAmount(laborFund);
        zusContributionRepository.save(zus);
    }

    @Override
    public void payZus(long zusId, LocalDate date) {
        ZusContribution zus = zusContributionRepository.findById(zusId).orElseThrow();
        zus.setPaymentDate(date);
        zus.setStatus(PAID);
    }
}
