package pl.stendbeck.taxhelper.service;

import pl.stendbeck.taxhelper.model.entity.Customer;

public interface CustomerService {

    Customer findByLogin(String login);

}
