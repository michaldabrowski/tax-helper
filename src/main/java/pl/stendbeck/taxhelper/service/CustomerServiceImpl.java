package pl.stendbeck.taxhelper.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.stendbeck.taxhelper.model.entity.Customer;
import pl.stendbeck.taxhelper.repository.CustomerRepository;

import javax.transaction.Transactional;

@Service
@Transactional
public class CustomerServiceImpl implements CustomerService {


    @Autowired
    private CustomerRepository customerRepository;


    @Override
    public Customer findByLogin(String login) {
        return customerRepository.findByLogin(login).orElseThrow();
    }
}
