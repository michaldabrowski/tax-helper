package pl.stendbeck.taxhelper.service;

import pl.stendbeck.taxhelper.model.entity.Customer;
import pl.stendbeck.taxhelper.model.entity.Month;

public interface TaxService {

    void createTaxesForMonth(Customer customer, Month month);

    void calculateMonth(Month month, boolean closeMonth);

}
