package pl.stendbeck.taxhelper.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import pl.stendbeck.taxhelper.model.entity.Invoice;

import java.util.List;

@Repository
public interface InvoiceRepository extends CrudRepository<Invoice, Long> {

    List<Invoice> findByMonthId(long id);
}
