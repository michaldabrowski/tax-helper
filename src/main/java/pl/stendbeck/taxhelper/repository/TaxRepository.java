package pl.stendbeck.taxhelper.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import pl.stendbeck.taxhelper.model.TaxType;
import pl.stendbeck.taxhelper.model.entity.Tax;

import java.util.List;

@Repository
public interface TaxRepository extends JpaRepository<Tax, Long> {

    Tax findByMonthIdAndType(long monthId, TaxType type);

    @Query("SELECT t " +
            "FROM Tax t " +
            "LEFT JOIN t.month m " +
            "LEFT JOIN m.year y " +
            "WHERE y.id = :year " +
            "AND t.type = 'VAT_TAX' " +
            "AND t.status != 'OPEN'")
    List<Tax> findByYearVat(long year);

    @Query("SELECT t " +
            "FROM Tax t " +
            "LEFT JOIN t.month m " +
            "LEFT JOIN m.year y " +
            "WHERE y.id = :year " +
            "AND t.type = 'INCOME_TAX' " +
            "AND t.status != 'OPEN'")
    List<Tax> findByYearIncome(long year);
}
