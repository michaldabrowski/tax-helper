package pl.stendbeck.taxhelper.model

import spock.lang.Specification

class VatValueSpec extends Specification{

    def "method should return 'Fizz' for number which is divided by three" () {
        given: "The customer gave us the number"
        def number = 3
        when: "The customer runs 'check' method"
        def result = VatValue._0_PERCENT.getVatAmount(number)
        then: "result should be 'Fizz'"
        result == 0
    }
}
